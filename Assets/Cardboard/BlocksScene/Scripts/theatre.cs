﻿using System;
using UnityEngine;
using Immersv;
using System.Collections;

public class theatre : MonoBehaviour
{

    private const string APPLICATION_ID = "5583-7919-0099";
    private const string PLACEMENT_ID = "7171-6115-0100";
    private bool _isSDKReady = false;

    //public GameObject Theatre;
    //private GameObject cloneTheatre;

    GameObject mainCamera;

    // Use this for initialization
    void Start()
    {
        mainCamera = GameObject.FindWithTag("MainCamera");
        PrepareSDK();
        
    }

    void PrepareSDK()
    {
        ImmersvSDK.OnInitSuccess += () =>
        {
            _isSDKReady = true;
            //cloneTheatre = Instantiate(Theatre, transform.position, transform.rotation) as GameObject;

            ImmersvSDK.Ads.OnAdReady += () =>
            {
                ImmersvSDK.Ads.StartAd();
            };

            ImmersvSDK.Ads.OnAdComplete += (AdViewResult result) =>
            {
                if (result.BillableCriteriaMet)
                {
                    //RewardUser();
                }
            };

        };
        ImmersvSDK.Init(APPLICATION_ID);
    }
    
    void ShowAd()
    {
        if (!_isSDKReady)
        {
            return;
        }
        ImmersvSDK.Ads.LoadAd(PLACEMENT_ID);
    }

    // Update is called once per frame
    void Update()
    {

        if (Cardboard.SDK.Triggered)
        {
            int x = Screen.width / 2;
            int y = Screen.height / 2;

            Ray ray = mainCamera.GetComponent<Camera>().ScreenPointToRay(new Vector3(x, y));
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.tag == "theatre")
                {
                    ShowAd();
                }
            }
        }

    }
}
