﻿// Copyright 2014 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]

public class PickupObject : MonoBehaviour {

	GameObject mainCamera;
	bool carrying;
	GameObject carriedObject;
	public float distance;
	public float smooth;
	// From cardboard demo
	private Vector3 startingPosition;

//	private float loLim = 0.005F;
//	private float hiLim = 0.3F;
//	private int steps = 0;
//	private bool stateH = false;
//	private float fHigh = 8.0F;
//	private float curAcc= 0F;
//	private float fLow = 0.2F;
//	private float avgAcc;
//
//	public int stepDetector() {
//		curAcc = Mathf.Lerp (curAcc, Input.acceleration.magnitude, Time.deltaTime * fHigh);
//		avgAcc = Mathf.Lerp (avgAcc, Input.acceleration.magnitude, Time.deltaTime * fLow);
//		float delta = curAcc - avgAcc;
//		if (!stateH) { 
//			if (delta > hiLim) {
//				stateH = true;
//				steps++;
//			} else if (delta < loLim) {
//				stateH = false;
//			}
//			stateH = false;
//		}
//		avgAcc = curAcc;
//
//		calDistance (steps);
//
//		print (steps);
//
//		return steps;
//	}
//
//	void calDistance (int steps) {
//		if (steps > 5) {
//			GetComponent<Renderer>().material.color = Color.red;
//		}
//	}



	void Start() {
		mainCamera = GameObject.FindWithTag("MainCamera");

		// From cardboard demo
		startingPosition = transform.localPosition;
		//		SetGazedAt(false);
	}

	// From cardboard demo
	void LateUpdate() {
		Cardboard.SDK.UpdateState();
		if (Cardboard.SDK.BackButtonPressed) {
			Application.Quit();
		}

		//stepDetector ();

	}

	//	public void SetGazedAt(bool gazedAt) {
	//		GetComponent<Renderer>().material.color = gazedAt ? Color.green : Color.red;
	//	}

	public void Reset() {
		transform.localPosition = startingPosition;
	}

	public void ToggleVRMode() {
		Cardboard.SDK.VRModeEnabled = !Cardboard.SDK.VRModeEnabled;
	}

	//	public void TeleportRandomly() {
	//	Vector3 direction = Random.onUnitSphere;
	//	direction.y = Mathf.Clamp(direction.y, 0.5f, 1f);
	//	float distance = 2 * Random.value + 1.5f;
	//	transform.localPosition = direction * distance;
	//	}

	//public float speed = 10.0F;

	// Update is called once per frame
	void Update() {
		if (carrying) {
			carry(carriedObject);
			CheckDrop();
		} else {
			Pickup ();
		}

		//- Returns list of acceleration measurements which occured during the last frame. (Read Only) (Allocates temporary variables)
//		Vector3 acceleration = Vector3.zero;
//		foreach (AccelerationEvent accEvent in Input.accelerationEvents) {
//			acceleration += accEvent.acceleration * accEvent.deltaTime;
//		}
//		if (acceleration.z > 0f || acceleration.x > 0f ) {
//			GetComponent<Renderer>().material.color = Color.red;
//		}
//		print(acceleration);

		//- Last measured linear acceleration of a device in three-dimensionl space. (Read only)
//		Vector3 dir = Vector3.zero;
//		dir.x = -Input.acceleration.y;
//		dir.z = Input.acceleration.x;
//		if (dir.sqrMagnitude > 1)
//			dir.Normalize ();
//
//		dir *= Time.deltaTime;
//		transform.Translate (dir * speed);

	}

	void carry(GameObject o) {
		o.transform.position = Vector3.Lerp(o.transform.position, mainCamera.transform.position + mainCamera.transform.forward * distance, Time.deltaTime * smooth);
		o.transform.rotation = Quaternion.identity;
	}

	public void Pickup() {
		//if (Input.GetKeyDown (KeyCode.E)) {
		if (Cardboard.SDK.Triggered) {
            
			int x = Screen.width / 2;
			int y = Screen.height / 2;

			Ray ray = mainCamera.GetComponent<Camera>().ScreenPointToRay(new Vector3(x,y) );
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit) ) {
                
                Pickupable p = hit.collider.GetComponent<Pickupable>();

				if (p != null) {
                    GetComponent<Renderer>().material.color = Color.green;
                    carrying = true;
					carriedObject = p.gameObject;
					p.gameObject.GetComponent<Rigidbody>().useGravity = false;
				}
			}
		}
		//}
	}

	public void CheckDrop() {
		//if (Input.GetKeyDown (KeyCode.E)) {
		if (Cardboard.SDK.Triggered) {
			dropObject();
		}
		//}
	}

	void dropObject() {
		GetComponent<Renderer>().material.color = Color.white;
		carrying = false;
		carriedObject.gameObject.GetComponent<Rigidbody>().useGravity = true;
		carriedObject = null;
	}

}
